// generate function name based on dir name and filename
export const genFunctionName = (filename: string): string => {
    const fileName = filename
        .split('/')
        .slice(-1)[0]
        .split('.')[0];
    return fileName;
};