import { base } from "../../common/module";
import * as decryptService from "./v1/services/decrypt.service";
import * as encryptService from "./v1/services/encrypt.service";
import * as registerService from "./v1/services/register.service";
import * as stateService from "./v1/services/state.service";

const router = base.initRouter();
router.post('/v1/register', registerService.register);
router.post('/v1/state', stateService.state);
router.post('/v1/decrypt', decryptService.decrypt);
router.post('/v1/encrypt', encryptService.encrypt);

export const auth = base.initModule(__filename,router);