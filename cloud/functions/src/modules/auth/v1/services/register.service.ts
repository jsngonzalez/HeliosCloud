
import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as registerController from "../controllers/register.controller";
import { DeviceModel } from '../models/device.model';

export { register };

async function register(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data) as DeviceModel;
        const response =  await registerController.register(object);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Dispositivo no valido" }));
    }
}
