import { encryptResponse, RequestService, ResponseService } from '../../../../common/service';

export { encrypt };

async function encrypt(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        return res.status(200).send(encryptResponse({ error: 0, response: data }));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send({ error: 1, response: "Error interno" + error });
    }
}
