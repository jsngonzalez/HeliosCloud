import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as stateController from "../controllers/state.controller";
import { DeviceModel } from '../models/device.model';

export { state };

async function state(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data) as DeviceModel;
        const response =  await stateController.state(object);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
