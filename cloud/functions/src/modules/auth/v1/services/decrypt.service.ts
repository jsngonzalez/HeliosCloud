import { decryptRequest, RequestService, ResponseService } from '../../../../common/service';

export { decrypt };

async function decrypt(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data);
        return res.status(200).send({ error: 0, response: object });
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send({ error: 1, response: "Error interno" + error });
    }
}
