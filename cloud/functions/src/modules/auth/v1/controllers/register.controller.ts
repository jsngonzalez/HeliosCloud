
import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { ApplicationModel } from "../models/application.model";
import { DeviceModel, OperationSystemType, validateDevice } from "../models/device.model";

export { register };

async function register(data:DeviceModel):Promise<ResponseModel> {
    try{
        const validate = validateDevice(data);
        if (validate.error === 0){
            const applicationdb =  await firebase.db.collection("applications").doc(data.uidApplication).get();
            if(applicationdb.exists) {
                const application = applicationdb.data() as ApplicationModel;
                data.activateSDK = false;
                data.lastUpdate = new Date;
                switch (data.operationSystem) {
                    case OperationSystemType.ios:
                        application.devices.ios += 1;
                        break;
                    case OperationSystemType.android:
                        application.devices.android += 1;
                        break;
                    case OperationSystemType.web:
                        application.devices.web += 1;
                        break;
                    default: break;
                }

                const deviceRef = firebase.db
                .collection("applications")
                .doc(application.uid)
                .collection("devices")
                .doc(data.uid);
                
                const applicationRef = firebase.db
                .collection("applications")
                .doc(application.uid);

                const batch = firebase.db.batch();
                batch.set(deviceRef, data);
                batch.update(applicationRef, application);
                await batch.commit();

                return {error:0,response: { activateSDK: data.activateSDK }};
            }else{
                return {error:1,response: "La aplicación no se encuentra registrada"}; 
            }
        }else{
            return validate
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}
