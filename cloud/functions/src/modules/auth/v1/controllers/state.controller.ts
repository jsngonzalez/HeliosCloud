import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { DeviceModel, validateDevice } from "../models/device.model";

export { state };

async function state(data:DeviceModel):Promise<ResponseModel> {
    try{
        const validate = validateDevice(data);
        if (validate.error === 0){

            const deviceRef = firebase.db
            .collection("applications")
            .doc(data.uidApplication)
            .collection("devices")
            .doc(data.uid)

            const devicedb =  await deviceRef.get();
            
            if(devicedb.exists) {
                const device = devicedb.data() as DeviceModel;
                device.lastUpdate = new Date;
                const batch = firebase.db.batch();
                batch.update(deviceRef, device);
                await batch.commit();
                return {error:0,response: { activateSDK: device.activateSDK }};
            }else{
                return {error:1,response: "La aplicación no se encuentra registrada"}; 
            }
        }else{
            return validate
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

