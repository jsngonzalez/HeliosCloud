export interface ApplicationModel {
    devices: NumberDeviceModel;
    managers: string[];
    uid: string;
    name: string;
}

export interface NumberDeviceModel {
    android: number;
    ios: number;
    web: number;
}