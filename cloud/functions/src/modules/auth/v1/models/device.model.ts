import { ResponseModel } from "../../../../common/controller";

export interface DeviceModel {
    uid: string;
    brand: string;
    reference: string;
    operationSystem: OperationSystemType;
    versionSDK: string;
    uidApplication: string;
    activateSDK: boolean;
    lastUpdate: Date;
}

export enum OperationSystemType {
    android = "ANDROID",
    ios = "IOS",
    web = "WEB"
}

export function validateDevice(data:DeviceModel): ResponseModel {
    if (data.uid === undefined || data.uid === ""){
        return {error:1,response: "El dispositivo no existe"};
    }

    if (data.uidApplication === undefined || data.uidApplication === ""){
        return {error:1,response: "La aplicación no existe o esta desactivada"};
    }
    
    return {error:0,response: ""}
}