import { base } from "../../common/module";
import * as appService from "./v1/services/app.service";

const router = base.initRouter();
router.get('/v1/:uidUser/apps', appService.getAll);
router.post('/v1/login', appService.login);

export const admin = base.initModule(__filename,router);