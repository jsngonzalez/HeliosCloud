import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { ApplicationModel, LoginModel } from "../models/app.model";

export { getAll, login };

async function getAll(uidUser:string):Promise<ResponseModel> {
    try{
        const query =  await firebase.db
            .collection("applications")
            .where("managers","array-contains",uidUser)
            .get();
            
            if (query.empty) {
                return {error:0,response: []};
            }  

            const listApps: ApplicationModel[] = [];
            query.forEach(doc => {
                const application = doc.data() as ApplicationModel;
                listApps.push(application);
            });

            return {error:0,response: listApps};
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

async function login(data:LoginModel):Promise<ResponseModel> {
    try{
        const query =  await firebase.db
            .collection("users")
            .where("email","==",data.email)
            .where("password","==",data.password)
            .get();
            
            
        console.log("query.empty",query.empty);
            if (query.empty) {
                return {error:1,response: "Por favor valida tus datos."};
            }
            
            const user = query.docs[0];
            console.log("query user",JSON.stringify(user));
            return {error:0,response: user.id};
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

