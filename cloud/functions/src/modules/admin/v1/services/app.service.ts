import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as appController from "../controllers/app.controller";
import { LoginModel } from '../models/app.model';

export { getAll, login };

async function getAll(req: RequestService, res: ResponseService) {
    try {
        const uid = req.params.uidUser;
        const response =  await appController.getAll(uid);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}

async function login(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        console.log("data",data);
        const object = decryptRequest(data) as LoginModel;
        console.log("LoginModel",JSON.stringify(object));
        const response =  await appController.login(object);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
