import { ResponseModel } from "../../../../common/controller";

export interface ApplicationModel {
    uid: string;
    name: string;
    devices: AnalyticsDeviceModel;
}

export interface AnalyticsDeviceModel {
    android: number;
    ios: number;
    web: number;
}

export function validateApplication(data:ApplicationModel): ResponseModel {
    if (data.name === undefined || data.name === ""){
        return {error:1,response: "El nombre de la aplicación no se encuentra"};
    }
    return {error:0,response: ""}
}

export interface LoginModel{
    email: string;
    password: string;
}