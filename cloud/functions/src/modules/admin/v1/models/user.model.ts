import { ResponseModel } from "../../../../common/controller";

export interface UserDeviceModel {
    userIdentifier: string;
    uid: string;
}

export function validateUserDevice(data:UserDeviceModel): ResponseModel {
    if (data.userIdentifier === undefined || data.userIdentifier === ""){
        return {error:1,response: "El userIdentifier no se encuentra"};
    }
    return {error:0,response: ""}
}