import { base } from "../../common/module";
import * as screenshotService from "./v1/services/screenshot.service";
import * as traceService from "./v1/services/trace.service";

const router = base.initRouter();
router.post('/v1/:uidApplication/:uidDevice/screenshot', screenshotService.update);
router.post('/v1/:uidApplication/:uidDevice/request', traceService.create);

export const trace = base.initModule(__filename,router);