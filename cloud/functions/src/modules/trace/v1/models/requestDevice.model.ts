import { ResponseModel } from "../../../../common/controller";

export interface RequestDeviceModel {
    url: string;
    headers: any;
    request: any;
    response: any;
    method: RequestType;
    date: Date;
}

export enum RequestType {
    get = "GET",
    post = "POST",
    put = "PUT",
    delete = "DELETE",
}

export function validateRequest(data:RequestDeviceModel): ResponseModel {
    if (data.url === undefined || data.url === ""){
        return {error:1,response: "No se recibio la url"};
    }
    return {error:0,response: ""}
}