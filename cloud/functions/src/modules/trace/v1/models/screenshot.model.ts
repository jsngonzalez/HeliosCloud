import { ResponseModel } from "../../../../common/controller";

export interface ScreenshotModel {
    screenshot: string;
    lastUpdateScreenshot: Date;
}

export function validateScreenshot(data:ScreenshotModel): ResponseModel {
    if (data.screenshot === undefined || data.screenshot === ""){
        return {error:1,response: "No se recibio el Screenshot"};
    }
    return {error:0,response: ""}
}