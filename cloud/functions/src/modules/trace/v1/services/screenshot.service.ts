import { encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as screenshotController from "../controllers/screenshot.controller";
import { ScreenshotModel } from '../models/screenshot.model';

export { update };

async function update(req: RequestService, res: ResponseService) {
    try {
        const object = req.body.data as ScreenshotModel;
        const uidApplication = req.params.uidApplication;
        const uidDevice = req.params.uidDevice;
        const response =  await screenshotController.update(object, uidApplication, uidDevice);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
