import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as traceController from "../controllers/trace.controller";
import { RequestDeviceModel } from '../models/requestDevice.model';

export { create };

async function create(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data) as RequestDeviceModel;
        const uidApplication = req.params.uidApplication;
        const uidDevice = req.params.uidDevice;
        const response =  await traceController.create(object, uidApplication, uidDevice);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
