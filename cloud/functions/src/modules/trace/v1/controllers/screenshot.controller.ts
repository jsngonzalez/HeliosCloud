import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { ScreenshotModel, validateScreenshot } from "../models/screenshot.model";

export { update };

async function update(data:ScreenshotModel, uidApplication: string, uidDevice: string):Promise<ResponseModel> {
    try{
        const validate = validateScreenshot(data);
        if (validate.error === 0){
            data.lastUpdateScreenshot = new Date;
            
            const screenshotRef =  await firebase.db
            .collection("applications")
            .doc(uidApplication)
            .collection("devices")
            .doc(uidDevice)
            .collection("screenshot")
            .doc();
            
            const batch = firebase.db.batch();
            batch.set(screenshotRef, data);
            await batch.commit();

            return {error:0,response: { update: true }};
        }else{
            return validate
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

