import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { RequestDeviceModel, validateRequest } from "../models/requestDevice.model";

export { create };

async function create(data:RequestDeviceModel, uidApplication: string, uidDevice: string):Promise<ResponseModel> {
    try{
        const validate = validateRequest(data);
        if (validate.error === 0){
            data.date = new Date;
            
            const deviceRef =  await firebase.db
            .collection("applications")
            .doc(uidApplication)
            .collection("devices")
            .doc(uidDevice)
            .collection("trace")
            .doc();

            const batch = firebase.db.batch();
            batch.set(deviceRef, data);
            await batch.commit();

            return {error:0,response: { update: true }};
        }else{
            return validate
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

