import { base } from "../../common/module";
import * as attributesService from "./v1/services/attributes.service";
import * as findService from "./v1/services/find.service";

const router = base.initRouter();
router.post('/v1/:uidApplication/:uidDevice/attribute', attributesService.update);
router.post('/v1/:uidApplication/find', findService.find);

export const device = base.initModule(__filename,router);