import { ResponseModel } from "../../../../common/controller";

export interface AttributeModel {
    key: string;
    value: string;
}

export function validateAttributeDevice(data:AttributeModel): ResponseModel {
    if (data.key === undefined || data.key === ""){
        return {error:1,response: "La key del atributo no se encuentra"};
    }
    if (data.value === undefined || data.value === ""){
        return {error:1,response: "El value del atributo no se encuentra"};
    }
    return {error:0,response: ""}
}