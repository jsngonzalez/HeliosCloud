import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { AttributeModel, validateAttributeDevice } from "../models/attribute.model";

export { update };

async function update(data:AttributeModel, uidApplication: string, uidDevice: string):Promise<ResponseModel> {
    try{
        const validate = validateAttributeDevice(data);
        if (validate.error === 0){
            const deviceRef =  await firebase.db
            .collection("applications")
            .doc(uidApplication)
            .collection("devices")
            .doc(uidDevice);

            const batch = firebase.db.batch();
            const deviceUpdate = {} as any;
            deviceUpdate[data.key] = data.value;
            batch.update(deviceRef, deviceUpdate);
            await batch.commit();

            return {error:0,response: { update: true }};
        }else{
            return validate
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

