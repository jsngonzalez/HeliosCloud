import { ResponseModel } from "../../../../common/controller";
import { firebase } from "../../../../common/firebase";
import { DeviceModel } from "../../../../modules/auth/v1/models/device.model";

export { find };

async function find(userIdentifier:string, uidApplication: string):Promise<ResponseModel> {
    try{
        if (userIdentifier !== ""){
            const query =  await firebase.db
            .collection("applications")
            .doc(uidApplication)
            .collection("devices")
            .where("userIdentifier","==",userIdentifier)
            .get();

            if (query.empty) {
                return {error:0,response: []};
            }  

            const listDevices: DeviceModel[] = [];
            query.forEach(doc => {
                const device = doc.data() as DeviceModel;
                listDevices.push(device);
            });

            return {error:0,response: listDevices};
        }else{
            return {error:1,response: "Debe ingresar un userIdentifier valido" };
        }
    } catch(err){
        return {error:1,response: "Error interno." + err };
    }
}

