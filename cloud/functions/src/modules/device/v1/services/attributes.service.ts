import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import * as attributeController from "../controllers/attributes.controller";
import { AttributeModel } from '../models/attribute.model';

export { update };

async function update(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data) as AttributeModel;
        const uidApplication = req.params.uidApplication;
        const uidDevice = req.params.uidDevice;
        const response =  await attributeController.update(object, uidApplication, uidDevice);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
