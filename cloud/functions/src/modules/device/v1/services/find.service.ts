import { decryptRequest, encryptResponse, RequestService, ResponseService } from '../../../../common/service';
import { UserDeviceModel } from '../../../../modules/admin/v1/models/user.model';
import * as findController from "../controllers/find.controller";

export { find };

async function find(req: RequestService, res: ResponseService) {
    try {
        const data = req.body.data as string;
        const object = decryptRequest(data) as UserDeviceModel;
        const uidApplication = req.params.uidApplication;
        const response =  await findController.find(object.userIdentifier, uidApplication);
        return res.status(200).send(encryptResponse(response));
    } catch (error) {
        console.log(`[Campaign service]=>${error}`);
        return res.status(200).send(encryptResponse({ error: 1, response: "Error interno" + error }));
    }
}
