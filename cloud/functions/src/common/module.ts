import { Router } from 'express';
import { genFunctionName } from '../utils/functions';
import { IRoutes } from "./routes.model";


export const base = {
    initRouter: initRouter,
    initModule: initModule
}

function initRouter(): Router {
    return Router()
}

function initModule(fileName: string, router: Router): IRoutes {
    const module: IRoutes = {
        name: genFunctionName(fileName),
        router: router
    };
    return module
}

