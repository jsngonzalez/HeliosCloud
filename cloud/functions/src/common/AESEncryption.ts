import { environment } from "./environment";
export { encrypt, decrypt };

function encrypt(jsonObject: Object): string {
    const val = JSON.stringify(jsonObject);
    const key = Buffer.from(environment.aes.key, environment.aes.bufferEncryption);
    const iv = Buffer.from(environment.aes.iv, environment.aes.bufferEncryption);
    const crypto = require('crypto');
    const cipher = crypto.createCipheriv(environment.aes.encryptionType, key, iv);
    let encrypted = cipher.update(val, environment.aes.bufferEncryption, environment.aes.encryptionEncoding);
    encrypted += cipher.final(environment.aes.encryptionEncoding);
    return encrypted;
}

function decrypt(base64String: string): any {
    const buff = Buffer.from(base64String, environment.aes.encryptionEncoding);
    const key = Buffer.from(environment.aes.key, environment.aes.bufferEncryption);
    const iv = Buffer.from(environment.aes.iv, environment.aes.bufferEncryption);
    const crypto = require('crypto');
    const decipher = crypto.createDecipheriv(environment.aes.encryptionType, key, iv);
    const deciphered = decipher.update(buff) + decipher.final();
    return JSON.parse(deciphered);
}
