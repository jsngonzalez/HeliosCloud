import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
admin.initializeApp(functions.config().firebase);

const database = admin.database();
const db = admin.firestore();
db.settings({ timestampsInSnapshots: true });

export const firebase = {
  admin: admin,
  db: db,
  database: database,
  FieldValue: admin.firestore.FieldValue,
  functions: functions
}