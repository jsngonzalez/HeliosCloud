export interface UserApplicationModel {
    uid: string;
    userIdentifier: string;
}