import { Request, Response } from 'express';
import * as AesEncryption from './AESEncryption';
import { ResponseModel } from './controller';

export interface RequestService extends Request { }
export interface ResponseService extends Response { }


export function encryptResponse(data: ResponseModel): ResponseModel {
    const response = AesEncryption.encrypt(data.response);
    return { error: data.error, response: response };
}

export function decryptRequest(data: string): any {
    return AesEncryption.decrypt(data);
}