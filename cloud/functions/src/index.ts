import * as express from 'express';
import { firebase } from './common/firebase';
import { IRoutes } from './common/routes.model';
import { admin } from "./modules/admin/admin.module";
import { auth } from "./modules/auth/auth.module";
import { device } from "./modules/device/device.module";
import { trace } from "./modules/trace/trace.module";

const routes = [ auth, device, trace, admin ];

routes.forEach((routerObj: IRoutes) => {
    const app_express = express();
    app_express.use(routerObj.router);
    exports[routerObj.name] = firebase.functions.https.onRequest(app_express);
});